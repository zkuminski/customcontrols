module.exports = {
  "stories": [
    // "../src/**/*.stories.mdx",
    // "../src/**/*.stories.@(js|jsx|ts|tsx)",
    '../src/app/**/custom-controls/*.stories.ts',
    '../src/app/**/*.stories.ts'
  ],
  "addons": [
    "@storybook/addon-links",
    "@storybook/addon-essentials",
    '@storybook/addon-actions', 
    '@storybook/addon-notes',
    '@storybook/addon-controls'
  ]
}