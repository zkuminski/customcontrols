import { action } from '@storybook/addon-actions';
import { Story } from '@storybook/angular/types-6-0';
import { FileUploadUsageModule } from '../file-upload-usage/file-upload-usage.module';

const actionsData = {
  filesToUpload: action('filesToUpload'),
};

export default {
  title: 'Example/File Upload Usage',
  excludeStories: /.*Data$/,
};

const FileUploadUsageTemplate: Story = (args) => ({
  moduleMetadata: {
    imports: [FileUploadUsageModule],
  },
  template: `<app-file-upload-usage></app-file-upload-usage>`,
  props: { ...args, ...actionsData },
});

export const FileUploadUsage = FileUploadUsageTemplate.bind({});
FileUploadUsage.args = {
  endpointUrl: '/api/batch/13956/attachments',
};
