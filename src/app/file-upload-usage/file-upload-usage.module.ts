import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FileUploadUsageComponent } from './file-upload-usage.component';
import { ButtonModule } from '../custom-controls/button/button.module';
import { FileUploadModule } from '../custom-controls/file-upload/file-upload.module';
import { InputModule } from '../custom-controls/input/input.module';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { ErrorModule } from '../custom-controls/error/error.module';

@NgModule({
  imports: [
    CommonModule,
    FileUploadModule,
    ButtonModule,
    InputModule,
    FormsModule,
    HttpClientModule,
    ErrorModule,
  ],
  declarations: [FileUploadUsageComponent],
  exports: [FileUploadUsageComponent],
})
export class FileUploadUsageModule {}
