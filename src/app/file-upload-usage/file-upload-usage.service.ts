import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class FileUploadUsageService {
  constructor(private httpClient: HttpClient) {}

  public uploadFile(
    requestUrl: string,
    file: File,
    params: any
  ): Observable<any> {
    const formData = new FormData();
    formData.append('file', file);
    return this.httpClient.post(requestUrl, formData, { params });
  }
}
