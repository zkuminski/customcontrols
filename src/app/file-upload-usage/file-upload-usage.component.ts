import { HttpParams } from '@angular/common/http';
import { Component, Input } from '@angular/core';
import { FileUploadUsageService } from './file-upload-usage.service';
import { FileError } from '../custom-controls/error/error.component';

@Component({
  selector: 'app-file-upload-usage',
  templateUrl: './file-upload-usage.component.html',
  styleUrls: ['./file-upload-usage.component.scss'],
})
export class FileUploadUsageComponent {
  @Input() endpointUrl = '/api/batch/13956/attachments';
  allowedFileTypes = '.pdf';
  allowedFileSize = 30;
  files: File[] = [];
  comment: string = '';

  constructor(private fileUploadUsageService: FileUploadUsageService) {}
  fileUploadStatus: FileError = { isValid: true, errorMessage: '' };
  filesToUpload($event: any) {
    this.files = $event;
  }

  uploadFile() {
    const params = {
      comment: this.comment,
    };
    this.fileUploadUsageService
      .uploadFile(this.endpointUrl, this.files[0], params)
      .subscribe();
  }

  fileStatus($event: any) {
    this.fileUploadStatus = $event;
  }
}
