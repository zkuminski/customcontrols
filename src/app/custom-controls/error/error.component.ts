import { Component, Input } from '@angular/core';

export type FileError = {
  isValid: boolean,
  errorMessage: string
}

@Component({
  selector: 'app-error',
  templateUrl: './error.component.html',
  styleUrls: ['./error.component.scss']
})
export class ErrorComponent {
  @Input() status!: FileError;

}
