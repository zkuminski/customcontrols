import { Directive, HostBinding } from '@angular/core';

@Directive({
  selector: 'input[customInput]',
})
export class InputDirective {
  @HostBinding('class') classes = this.getInputClass();

  private getInputClass() {
    return 'custom-input';
  }
}
