import { Story } from '@storybook/angular/types-6-0';
import { InputModule } from './input.module';

export default {
  title: 'Custom Controls/Input',
  excludeStories: /.*Data$/,
};

const Template: Story = (args) => ({
  moduleMetadata: {
    imports: [InputModule],
  },
  template: `<input customInput />`,
  props: { ...args },
});

export const Input = Template.bind({});
Input.args = {};
