import { Injectable } from '@angular/core';
import { FileError } from '../../error/error.component';

@Injectable({
  providedIn: 'root'
})
export class FileUploadService {

  public prepareListOfValidTypes(allowedTypesString: string): string[] {
    return allowedTypesString.split(',');
  }

  public ifFileIsValid(fileName: string, filesValidTypes: string[], fileSize: number, allowedFileSize: number): FileError {
    const fileStatus: FileError = {
      errorMessage: '',
      isValid: true
    };

    if (this.isFileExtensionValid(fileName, filesValidTypes)) {
      if (this.isFileSizeValid(fileSize, allowedFileSize)) {
       }
      else {
        fileStatus.isValid = false;
        fileStatus.errorMessage = 'Selected file is too big. (Max. ' + allowedFileSize + 'MB)';
        return fileStatus;
      }
    } else {
      fileStatus.isValid = false;
      fileStatus.errorMessage = 'This file cannot be uploaded as attachment - the only allowed file type(s): ' + filesValidTypes.join(',');
      return fileStatus;
    }
    return fileStatus;
  }

  private isFileExtensionValid(fileName: string, filesValidTypes: string[]) {
    const extension = fileName.slice(fileName.lastIndexOf('.'), fileName.length + 1);
    return filesValidTypes.indexOf(extension) > -1
      ? true
      : false;
  }

  private isFileSizeValid(fileSize: number, allowedFileSize: number): boolean {
    return fileSize <= (allowedFileSize * Math.pow(1024, 2))
      ? true
      : false;
  }
}
