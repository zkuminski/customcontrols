import { action } from '@storybook/addon-actions';
import { Story } from '@storybook/angular/types-6-0';
import { ButtonModule } from '../button/button.module';
import { InputModule } from '../input/input.module';
import { FileUploadModule } from './file-upload.module';

const actionsData = {
  filesToUpload: action('filesToUpload'),
};

export default {
  title: 'Custom Controls/File Upload',
  excludeStories: /.*Data$/,
};

const FileUploadButtonTemplate: Story = (args) => ({
  moduleMetadata: {
    imports: [FileUploadModule, ButtonModule, InputModule],
  },
  template: ` 
  <app-file-upload 
      [allowedFileTypes]="allowedFileTypes"
      [allowedFileSize]="allowedFileSize"
      (filesToUpload)="filesToUpload($event)">
    <button custom-primary-button>{{ uploadButtonText }}</button>
  </app-file-upload>
  `,
  props: { ...args, ...actionsData },
});

export const FileUpload = FileUploadButtonTemplate.bind({});
FileUpload.args = {
  uploadButtonText: '+ Upload',
  allowedFileTypes: '.pdf', // expample '.pdf,.txt,.doc' for PDF, TEXT and MS Office DOC files
  allowedFileSize: 30, // MB;
};
