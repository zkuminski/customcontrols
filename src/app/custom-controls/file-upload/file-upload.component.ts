import {
  Component,
  ElementRef,
  EventEmitter,
  Input,
  Output,
  ViewChild,
} from '@angular/core';
import { FileError } from '../error/error.component';
import { FileUploadService } from './services/file-upload.service';

@Component({
  selector: 'app-file-upload',
  templateUrl: './file-upload.component.html',
  styleUrls: ['./file-upload.component.scss'],
})
export class FileUploadComponent {
  @Input() public allowedFileTypes = '.pdf,.txt'; // expample '.pdf,.txt,.doc' for PDF, TEXT and MS Office DOC files
  @Input() public allowedFileSize = 30; // in MB

  @Output() filesToUpload: EventEmitter<any> = new EventEmitter();
  @Output() fileErrorStatus: EventEmitter<any> = new EventEmitter();

  @ViewChild('fileInput') fileInput!: ElementRef;
  files: File[] = [];
  fileUploadStatus: FileError = { isValid: true, errorMessage: '' };

  constructor(private fileUploadService: FileUploadService) { }

  public fileInputChange($event: any): void {
    this.files = this.prepareListOfValidFiles($event.target.files);
    this.filesToUpload.emit(this.files);
    this.fileErrorStatus.emit(this.fileUploadStatus);
    this.fileInput.nativeElement.value = '';
  }

  private prepareListOfValidFiles(files: FileList): File[] {
    const validFiles = [];
    const filesValidTypes = this.fileUploadService.prepareListOfValidTypes(this.allowedFileTypes);
    this.fileUploadStatus = this.fileUploadService.ifFileIsValid(files[0].name, filesValidTypes, files[0].size, this.allowedFileSize);
    if (this.fileUploadStatus.isValid) {
      validFiles.push(files[0]);
    }
    return validFiles;
  }
}
