import { Component, ElementRef, HostBinding } from '@angular/core';

@Component({
  selector: `
    button[custom-primary-button],
  `,
  styleUrls: ['./button.component.scss'],
  template: `<span><ng-content></ng-content></span>`,
})
export class ButtonComponent {
  @HostBinding('class') classes = this.getButtonClass();

  private static buttonHostAttributesToClassMapper = {
    'custom-primary-button': 'custom-button--primary',
  };

  constructor(private readonly elementRef: ElementRef) {}

  private getButtonClass(): string {
    const classesMappedFromHostAttributes = Object.entries(
      ButtonComponent.buttonHostAttributesToClassMapper
    )
      .filter(([hostAttribute]) => this.hostElement.hasAttribute(hostAttribute))
      .map(([, buttonClass]) => buttonClass);

    return classesMappedFromHostAttributes[0];
  }

  get hostElement(): HTMLButtonElement {
    return this.elementRef.nativeElement as HTMLButtonElement;
  }
}
