import { Story } from '@storybook/angular/types-6-0';
import { ButtonModule } from './button.module';

const defaultArgs = {};

export default {
  title: 'Custom Controls/Button',
  excludeStories: /.*Data$/,
};

const Template: Story = (args) => ({
  moduleMetadata: {
    imports: [ButtonModule],
  },
  template: `
  <button custom-primary-button>{{ buttonContentText }}</button>`,
  props: { ...args },
});

export const Button = Template.bind({});
Button.args = { buttonContentText: 'Button' };
